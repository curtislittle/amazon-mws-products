Amazon Marketplace Web Service Products PHP Client Library
=================================================

Installation
------------

Install [Composer](http://getcomposer.org/) and add amazon-mws-reports to your `composer.json` by running this command:

    composer require curtislittle/amazon-mws-products

Version
-------

Amazon Marketplace Web Service Products PHP Client Library - Version 2011-10-01
